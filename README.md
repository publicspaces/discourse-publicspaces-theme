# PublicSpaces Discourse Forum Theme
A minimal forum theme without externaly hosted Google fonts.
Forked from: https://github.com/discourse/minima

Learn more
https://meta.discourse.org/t/minima-a-minimal-theme-for-discourse/108178
